#ifndef DUNE_VECTORCLASS_VECARRAY_HH
#define DUNE_VECTORCLASS_VECARRAY_HH

#include <dune/vectorclass/vectorclass.hh>

namespace Dune {
  namespace Simd {
    namespace VectorclassImpl {
      template<class T, size_t n, size_t m = 1, class=void>
      struct VecArrayChooser;

#if MAX_VECTOR_SIZE >= 512
      template<size_t n, size_t m>
      struct VecArrayChooser<double, n, m,
                             std::enable_if_t<n%8==0 && (n>=m*8)>>
      {
        typedef LoopSIMD<Vec8d, n/8> type;
      };

      template<>
      struct VecArrayChooser<double, 8, 1>{
        typedef Vec8d type;
      };
#endif
#if MAX_VECTOR_SIZE >= 256
      template<size_t n, size_t m>
      struct VecArrayChooser<double, n, m,
                             std::enable_if_t<n%4==0 &&
                                              (MAX_VECTOR_SIZE==256 || n%8!=0 || (n<m*8)) &&
                                              (n>=m*4)>>{
        typedef LoopSIMD<Vec4d, n/4> type;
      };

      template<>
      struct VecArrayChooser<double, 4, 1>{
        typedef Vec4d type;
      };
#endif

#if MAX_VECTOR_SIZE >= 128
      template<size_t n, size_t m>
      struct VecArrayChooser<double, n, m,
                             std::enable_if_t<n%2==0 && (MAX_VECTOR_SIZE==128 || n%4!=0 || (n<m*4)) &&
                                              (n>=m*2)>>{
        typedef LoopSIMD<Vec2d, n/2> type;
      };

      template<>
      struct VecArrayChooser<double, 2, 1>{
        typedef Vec2d type;
      };
#endif

      template<size_t n, size_t m>
      struct VecArrayChooser<double, n, m, std::enable_if_t<n%2!=0 || (n<m*2)>>
      {
        typedef LoopSIMD<double,n> type;
      };

      template<>
      struct VecArrayChooser<double, 1>
      {
        typedef double type;
      };
    }

    template<class T, size_t n, size_t m=1>
    using VecArray = typename VectorclassImpl::VecArrayChooser<T,n,m>::type;
  }
}

#endif
