# File for module specific CMake tests.
find_package(Git QUIET)

option(GIT_SUBMODULE "Check submodules during build" ON)

option(APPLY_VCL_PATCH "Apply vectorclass patch" ON)

function(git_update_submodules)
    if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
        # Update submodules as needed
        message(STATUS "Submodule update")
        execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
                        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                        RESULT_VARIABLE GIT_SUBMOD_RESULT)
        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
            message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
        endif()
    endif()
endfunction()

function(git_apply_patch WORKING_DIR PATCH)
    message(STATUS "Apply vectorclass patch")
    execute_process(COMMAND ${GIT_EXECUTABLE} apply --whitespace=fix ${PROJECT_SOURCE_DIR}/${PATCH}
                    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/${WORKING_DIR}
                    OUTPUT_VARIABLE GIT_STATUS
                    RESULT_VARIABLE GIT_APPLY_RESULT)
    if(NOT ${GIT_APPLY_RESULT} EQUAL "0")
        message(FATAL_ERROR "git apply_patch failed with ${GIT_SUBMOD_RESULT}, could not apply patch")
    endif()
endfunction()
